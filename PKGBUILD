# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>
# Contributor: Alonso Rodriguez <alonsorodi20 (at) gmail (dot) com>
# Contributor: Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: Felix Yan <felixonmars@archlinux.org>
# Contributor: Thomas Baechler <thomas@archlinux.org>
# Contributor: loqs
# Contributor: Jonathon

_linuxprefix=linux516-rt
_extramodules=extramodules-5.16-rt-MANJARO
# don't edit here
pkgver=340.108_5.16.2_rt19_2

_nver=340
# edit here for new version
_sver=108
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-${_nver}xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
makedepends=("$_linuxprefix" "$_linuxprefix-headers" "nvidia-${_nver}xx-utils=${_pkgver}")
groups=("$_linuxprefix-extramodules")
provides=("nvidia=$pkgver" "nvidia-${_nver}xx-modules")
conflicts=("nvidia-${_nver}xx-dkms" "$_linuxprefix-nvidia" "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-418xx" "$_linuxprefix-nvidia-430xx"
           "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx" "$_linuxprefix-nvidia-455xx"
           "$_linuxprefix-nvidia-460xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="http://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run"
        '0001-kernel-5.7.patch' '0002-kernel-5.8.patch' '0003-kernel-5.9.patch'
        '0004-kernel-5.10.patch' '0005-kernel-5.11.patch' '0006-kernel-5.14.patch'
        '0007-kernel-5.15.patch' '0008-kernel-5.16.patch')
sha256sums=('995d44fef587ff5284497a47a95d71adbee0c13020d615e940ac928f180f5b77'
            'c8bda5fb238fbebc5bf6ae4b7646e48b30a96b9060ced20d93c53c14ac3161f6'
            '10b91c8dbc269ff1d8e3e8a1866926c309ff3912d191a05cd5724a3139776f32'
            'e06af37ffa2203698594e0f58816b809feced9b2374927e13b85fd5c18fa3114'
            '5e184ca5fcbf5071050f23503bfd3391c4bc1ccc31453338791a3da3885b6085'
            '5a95509d451719fe32a87c588a100b840ef84bc4ba49989c67ae00103f1becf7'
            '47ca88252c6b40f488f403f81c3eb1c1e5a5eed1dc353e31d53b5c815c433238'
            'ff4869ea16eb3d894b13a6ca6775906ce0feacf405a2ade63c4f052df6024769'
            'dea96a271aac29b963d68518e0be0f1f9e559f66790f8a43e0c515086218babe')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    # patches here
    # https://gitlab.com/taz007/nvidia-340xx
    patch -Np1 -i ../0001-kernel-5.7.patch
    patch -Np1 -i ../0002-kernel-5.8.patch
    patch -Np1 -i ../0003-kernel-5.9.patch
    patch -Np1 -i ../0004-kernel-5.10.patch
    patch -Np1 -i ../0005-kernel-5.11.patch
    patch -Np1 -i ../0006-kernel-5.14.patch
    patch -Np1 -i ../0007-kernel-5.15.patch
    patch -Np1 -i ../0008-kernel-5.16.patch

    export DISTCC_DISABLE=1
    export CCACHE_DISABLE=1
    export IGNORE_PREEMPT_RT_PRESENCE=1
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module

    cd uvm
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}" "nvidia-${_nver}xx-utils=${_pkgver}")

    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/uvm/nvidia-uvm.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
